﻿using UnityEngine;
using System.Collections;

public class SimpleCharacter : MonoBehaviour {

    public CharacterController controller;

    public float moveSpeed = 4.5f;
    public float gravity = 20;
    public float jumpTime = .5f;
    public float jumpTimer = -1;

    CollisionFlags collisionFlags = CollisionFlags.None;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        float vert = Input.GetAxis("Vertical");
        float horz = Input.GetAxis("Horizontal");

        Vector3 movement = new Vector3(horz, 0, vert);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            jumpTimer = jumpTime;
        }
        else if (Input.GetKeyUp(KeyCode.Space))
        {
            jumpTimer = -1;
        }

        movement *= moveSpeed;
        if (jumpTimer < 0)
        {
            if (collisionFlags != CollisionFlags.Below)
                movement.y = -gravity;
        }
        else
        {
            movement.y = 8;
            jumpTimer -= Time.deltaTime;
        }

        collisionFlags = controller.Move(movement * Time.deltaTime);
	}
}
