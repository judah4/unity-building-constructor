﻿using UnityEngine;
using System.Collections;

public class SimpleMove : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.W))
        {
            this.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + 5 * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.S))
        {
            this.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + -5 * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.A))
        {
            this.transform.position = new Vector3(transform.position.x + -5 * Time.deltaTime, transform.position.y, transform.position.z);
        }

        if (Input.GetKey(KeyCode.D))
        {
            this.transform.position = new Vector3(transform.position.x + 5 * Time.deltaTime, transform.position.y, transform.position.z);
        }

	}
}
