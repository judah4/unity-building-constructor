﻿using UnityEngine;
using System.Collections;

public class SimpleLevelManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetKeyDown(KeyCode.F1))
        {
            if (Application.levelCount > 0)
            {
                Application.LoadLevel(0);
            }
        }
        if (Input.GetKeyDown(KeyCode.F2))
        {
            if (Application.levelCount > 0)
            {
                Application.LoadLevel(1);
            }
        }
        if (Input.GetKeyDown(KeyCode.F3))
        {
            if (Application.levelCount > 1)
            {
                Application.LoadLevel(2);
            }
        }
        if (Input.GetKeyDown(KeyCode.F4))
        {
            if (Application.levelCount > 2)
            {
                Application.LoadLevel(3);
            }
        }
        if (Input.GetKeyDown(KeyCode.F5))
        {
            if (Application.levelCount > 3)
            {
                Application.LoadLevel(4);
            }
        }
        if (Input.GetKeyDown(KeyCode.F6))
        {
            if (Application.levelCount > 4)
            {
                Application.LoadLevel(5);
            }
        }
        if (Input.GetKeyDown(KeyCode.F7))
        {
            if (Application.levelCount > 5)
            {
                Application.LoadLevel(6);
            }
        }
        if (Input.GetKeyDown(KeyCode.F8))
        {
            if (Application.levelCount > 6)
            {
                Application.LoadLevel(7);
            }
        }
        if (Input.GetKeyDown(KeyCode.F9))
        {
            if (Application.levelCount > 7)
            {
                Application.LoadLevel(8);
            }
        }

	}
}
