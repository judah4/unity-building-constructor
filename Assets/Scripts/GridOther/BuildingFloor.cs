﻿using UnityEngine;
using System.Collections;

public class BuildingFloor : MonoBehaviour {

    public new Renderer renderer;
	public GameObject floorPrefab;
	public GameObject floor;
    public Texture texture;
    Texture startTexture;
    GameObject underside;

    bool hoverOver = true;
    Color startColor;

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        if (renderer != null)
        {
            if (hoverOver)
            {
                renderer.material.color = Color.yellow;
            }
            else
            {
                renderer.material.color = startColor;
            }
        }
        hoverOver = false;
	}

    public void HoverOver()
    {

        hoverOver = true;


    }

	public void CreateFloor(GameObject prefab) {
		if(prefab != floorPrefab) {
			floorPrefab = prefab;
			if(floor != null)
				Destroy(floor);
		}

		if(floor == null) {
			floor = Instantiate(floorPrefab) as GameObject;
			floor.transform.parent = transform;
			floor.transform.localPosition = Vector3.zero;
			floor.transform.localRotation = Quaternion.identity;
            floor.layer = 16;

            renderer = GetComponentInChildren<Renderer>();
            startColor = renderer.material.color;
            startTexture = renderer.material.mainTexture;
            if (texture != null)
            {
                renderer.material.mainTexture = texture;
            }
		}
    }

    public void ChangeTexture(Texture texture)
    {
            
        this.texture = texture;

        if (texture != null)
        {
            renderer.material.mainTexture = texture;
        }
        else
        {
            renderer.material.mainTexture = startTexture;
        }

    }

    public void CreateUnderside(GameObject prefab)
    {

        if (underside == null)
        {
            underside = Instantiate(prefab) as GameObject;
            underside.transform.parent = transform;
            underside.transform.localPosition = new Vector3(0, -.05f, 0);
            underside.transform.localRotation = Quaternion.Euler(0, 0, 180);
            
        }
    }
	
}
