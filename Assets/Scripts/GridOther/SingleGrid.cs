﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SingleGrid : MonoBehaviour {

    public GameObject highlightCube;
    public GameObject grid;
	public GameObject floorPrefab;
    public GameObject undersidePrefab;
	public GameObject wallPrefab;
	public GameObject postPrefab;
	public GameObject RoofPrefab;

    public List<GameObject> doorPrefabs = new List<GameObject>();
    public List<Texture> wallTextures = new List<Texture>();
    public List<Texture> floorTextures = new List<Texture>();
    

    

    public BuildingEditMode editMode = BuildingEditMode.Add;
    public bool hideGrid = false;
    public int doorIndex = 0;
    public int textureIndex = 0;
    public int floorTextureIndex = 0;
    public string saveName = "House1";
    public int floorLevel = 0;

	[HideInInspector]
	public GameObject building;


    Dictionary<int, FloorHolder> floorData = new Dictionary<int, FloorHolder>();
    List<BuildingRoof> roofs = new List<BuildingRoof>();
    new BoxCollider collider;

    ObjectScaler currentScaler;
    BuildingRoof currentScaled;

	//List<BuildingWall> walls = new List<BuildingWall>();

	//public List<Vector2> highlightAreas = new List<Vector2>();

	// Use this for initialization
	void Start () {
        collider = GetComponent<BoxCollider>();

		building = new GameObject("Building");
		building.transform.parent = transform;
		building.transform.localPosition = Vector3.zero;
		building.transform.localRotation = Quaternion.identity;

        highlightCube.renderer.material.color = Color.yellow;

        
	}
	
	// Update is called once per frame
	void Update () {

        if (floorData.ContainsKey(floorLevel) == false)
        {
            floorData.Add(floorLevel, new FloorHolder());
        }

        grid.transform.localPosition = new Vector3(0, floorLevel * 2 + 0.4f, 0);
        collider.center = new Vector3(0, floorLevel * 2 + -0.1f, 0);


        if (currentScaler != null)
        {
            if (editMode != BuildingEditMode.Roofs)
            {
                currentScaler.gameObject.SetActive(false);
                currentScaler = null;
                 
            }
            else
            {
                if (currentScaled != null)
                {
                    currentScaler.gameObject.SetActive(true);
                    currentScaled.transform.position = currentScaler.transform.position;
                    currentScaled.transform.rotation = currentScaler.transform.rotation;
                }
                else
                {
                    currentScaler.gameObject.SetActive(false);
                    currentScaler = null;
                    
                }
            }
        }
 
        

        if (Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.RightShift))
        {
            if (editMode == BuildingEditMode.Add)
            {
                editMode = BuildingEditMode.Destroy;
            }
            else if (editMode == BuildingEditMode.Destroy)
            {
                editMode = BuildingEditMode.Add;
            }
            if (editMode == BuildingEditMode.Walls)
            {
                editMode = BuildingEditMode.DestroyWalls;
            }
            else if (editMode == BuildingEditMode.DestroyWalls)
            {
                editMode = BuildingEditMode.Walls;
            }
     
        }

        if (Input.GetKeyUp(KeyCode.LeftShift) || Input.GetKeyUp(KeyCode.RightShift))
        {
            if (editMode == BuildingEditMode.Add)
            {
                editMode = BuildingEditMode.Destroy;
            }
            else if (editMode == BuildingEditMode.Destroy)
            {
                editMode = BuildingEditMode.Add;
            }
            if (editMode == BuildingEditMode.Walls)
            {
                editMode = BuildingEditMode.DestroyWalls;
            }
            else if (editMode == BuildingEditMode.DestroyWalls)
            {
                editMode = BuildingEditMode.Walls;
            }
        }

        Ray ray = Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.nearClipPlane));//new Ray(Camera.main.ScreenPointToRay(Input.mousePosition), Camera.main.transform.forward);
        RaycastHit info;

        
        if(editMode == BuildingEditMode.Add || editMode == BuildingEditMode.Destroy)
        {
            highlightCube.SetActive(true);
            if (Physics.Raycast(ray, out info, 200, 1 << 14))
            {
                Vector3 pos = transform.InverseTransformPoint(info.point);

                highlightCube.transform.localPosition = new Vector3(Mathf.RoundToInt(pos.x), floorLevel * 2, Mathf.RoundToInt(pos.z));

                highlightCube.transform.localScale = new Vector3(1, 1, 1);
            }
        }
        else if(editMode == BuildingEditMode.Walls || editMode == BuildingEditMode.DestroyWalls)
        {
            highlightCube.SetActive(true);
            if (Physics.Raycast(ray, out info, 200, 1 << 14))
            {
                Vector3 pos = transform.InverseTransformPoint(info.point);
                
                float x = RoundPosition(pos.x);
                float z = RoundPosition(pos.z);

                highlightCube.transform.localScale = new Vector3(.2f, 1, .2f);

                if (setStarting)
                {
                    if (Mathf.Abs(x - startingHighlightGrid.x) > Mathf.Abs(z - startingHighlightGrid.y))
                    {

                        //x = (int)pos.x + (pos.x > 0 ? .5f : -.5f);
                        //z = Mathf.RoundToInt(z);
                        z = startingHighlightGrid.y;

                        // highlightCube.transform.localRotation = Quaternion.Euler(0, 90, 0);


                    }
                    else  //snap to z
                    {
                        //z = (int)pos.z + (pos.z > 0 ? .5f : -.5f);
                        //x = Mathf.RoundToInt(x);
                        x = startingHighlightGrid.x;

                        // highlightCube.transform.localRotation = Quaternion.identity;


                    }
                }

                highlightCube.transform.localPosition = new Vector3(x, floorLevel * 2, z);
            }
        }
        else if (editMode == BuildingEditMode.Textures)
        {
            
            if (Physics.Raycast(ray, out info, 200, 1 << 15))
            {
                BuildingPiece piece = info.collider.transform.parent.GetComponent<BuildingPiece>();
                if (piece != null)
                {
                    piece.HoverOver(info.collider.gameObject);
                    if (Input.GetMouseButton(0) == true)
                    {
                        if (textureIndex == -1)
                        {
                            piece.ChangeTexture(null, info.collider.gameObject);
                        }
                        else
                        {
                            piece.ChangeTexture(wallTextures[textureIndex], info.collider.gameObject);
                        }
                    }
                }

            }
            
            highlightCube.SetActive(false);
        }
        else if (editMode == BuildingEditMode.Doors)
        {
            
                if (Physics.Raycast(ray, out info, 200, 1 << 15))
                {
                    BuildingPiece piece = info.collider.transform.parent.GetComponent<BuildingPiece>();

                    if (piece != null)
                    {
                        piece.HoverOver(info.collider.gameObject);
                        if (Input.GetMouseButton(0) == true)
                        {
                            if (doorIndex == -1)
                            {
                                piece.CreateVisual(wallPrefab);
                            }
                            else
                            {
                                piece.CreateVisual(doorPrefabs[doorIndex]);
                            }
                        }
                    }


                
            }
            highlightCube.SetActive(false);
        }
        else if (editMode == BuildingEditMode.WallTextures)
        {

            if (Physics.Raycast(ray, out info, 200, 1 << 16))
            {
                BuildingFloor piece = info.collider.transform.parent.GetComponent<BuildingFloor>();
                if (piece != null)
                {
                    piece.HoverOver();
                    if (Input.GetMouseButton(0) == true && !Input.GetKey(KeyCode.LeftShift))
                    {
                        if (floorTextureIndex == -1)
                        {
                            piece.ChangeTexture(null);
                        }
                        else
                        {
                            piece.ChangeTexture(floorTextures[floorTextureIndex]);
                        }
                    }
                    else
                    {
                        Vector3 pos = transform.InverseTransformPoint(info.point);

                        highlightCube.transform.localPosition = new Vector3(Mathf.RoundToInt(pos.x), floorLevel * 2, Mathf.RoundToInt(pos.z));

                        highlightCube.transform.localScale = new Vector3(1, 1, 1);
                    }
                }

            }

            highlightCube.SetActive(false);
        }
        else if (editMode == BuildingEditMode.Roofs)
        {

            if (Input.GetMouseButtonDown(0))
            {
                if (!Physics.Raycast(ray, out info, 200, 1 << 18)) {
                    if (Physics.Raycast(ray, out info, 200, 1 << 17))
                    {


                        BuildingRoof roof = info.collider.transform.parent.gameObject.GetComponent<BuildingRoof>();
                        if (roof != null)
                        {
                            if (roof != currentScaled)
                            {

                                currentScaled = roof;
                                currentScaler = roof.objectScaler;
                                //objectScaler.resetScale(currentScaler);
                            }
                        }
                    }
                    else if (Physics.Raycast(ray, out info, 200, 1 << 14))
                    {

                        Vector3 pos = transform.InverseTransformPoint(info.point);
                        Vector2 position = new Vector3(Mathf.RoundToInt(pos.x), Mathf.RoundToInt(pos.z));

                        roofs.Add(new GameObject("Roof").AddComponent<BuildingRoof>());
                        roofs[roofs.Count - 1].transform.parent = building.transform;
                        roofs[roofs.Count - 1].transform.localPosition = new Vector3(position.x, floorLevel * 2, position.y);
                        roofs[roofs.Count - 1].transform.localRotation = Quaternion.identity;

                        roofs[roofs.Count - 1].CreateRoof(RoofPrefab);
                        currentScaler = roofs[roofs.Count - 1].objectScaler;
                        currentScaled = roofs[roofs.Count - 1];
                        //objectScaler.resetScale();

                    }
                }
            }
            
        }

        HandleInput();
    

        if (hideGrid)
        {
            grid.SetActive(false);
            collider.enabled = false;
        }
        else
        {
            grid.SetActive(true);
            collider.enabled = true;
        }

	}

    void OnGUI()
    {
        hideGrid = GUI.Toggle(new Rect(Screen.width - 105, 5, 100, 30), hideGrid, "Hide Grid");

        if (GUI.Button(new Rect(Screen.width - 210, 5, 100, 30), "CombineMesh"))
        {
            CombineBuilding();
        }


        GUI.Label(new Rect(5, Screen.height - 80, 120, 40), "Building Level: " + (floorLevel + 1));

        if (GUI.Button(new Rect(200, Screen.height - 100, 40, 40), "+"))
        {
            floorLevel++;
        }
        if (floorLevel == 0)
        {
            GUI.Box(new Rect(200, Screen.height - 55, 40, 40), "-");
        }
        else if (GUI.Button(new Rect(200, Screen.height - 55, 40, 40), "-"))
        {
            floorLevel--;
        }


        if (editMode == BuildingEditMode.Add)
        {
            GUI.Box(new Rect(5, 5, 100, 30), "Floors");
        }
        else if (GUI.Button(new Rect(5, 5, 100, 30), "Floors"))
        {
            editMode = BuildingEditMode.Add;
        }

        if (editMode == BuildingEditMode.Destroy)
        {
            GUI.Box(new Rect(110, 5, 100, 30), "Destroy Floors");
        }
        else if (GUI.Button(new Rect(110, 5, 100, 30), "Destroy Floors"))
        {
            editMode = BuildingEditMode.Destroy;
        }

        if (editMode == BuildingEditMode.Walls)
        {
            GUI.Box(new Rect(220, 5, 100, 30), "Walls");
        }
        else if (GUI.Button(new Rect(220, 5, 100, 30), "Walls"))
        {
            editMode = BuildingEditMode.Walls;
        }

        if (editMode == BuildingEditMode.DestroyWalls)
        {
            GUI.Box(new Rect(330, 5, 100, 30), "Destroy Walls");
        }
        else if (GUI.Button(new Rect(330, 5, 100, 30), " Destroy Walls"))
        {
            editMode = BuildingEditMode.DestroyWalls;
        }

        if (editMode == BuildingEditMode.Doors)
        {
            GUI.Box(new Rect(440, 5, 100, 30), "Doors");

            if (doorIndex == -1)
            {
                GUI.Box(new Rect(5, 40, 100, 30), "Delete");
            }
            else if (GUI.Button(new Rect(5, 40, 100, 30), "Delete"))
            {
                doorIndex = -1;
            }

            for (int cnt = 0; cnt < doorPrefabs.Count; cnt++)
            {
                if (doorIndex == cnt)
                {
                    GUI.Box(new Rect(5, 75 + 35 * cnt, 100, 30), doorPrefabs[cnt].name);
                }
                else if (GUI.Button(new Rect(5, 75 + 35 * cnt, 100, 30), doorPrefabs[cnt].name))
                {
                    doorIndex = cnt;
                }
            }

        }
        else if (GUI.Button(new Rect(440, 5, 100, 30), "Doors"))
        {
            editMode = BuildingEditMode.Doors;
        }

        if (editMode == BuildingEditMode.Textures)
        {
            GUI.Box(new Rect(550, 5, 100, 30), "Wall Textures");

            if (textureIndex == -1)
            {
                GUI.Box(new Rect(5, 40, 100, 30), "Delete");
            }
            else if (GUI.Button(new Rect(5, 40, 100, 30), "Delete"))
            {
                textureIndex = -1;
            }

            for (int cnt = 0; cnt < wallTextures.Count; cnt++)
            {
                if (textureIndex == cnt)
                {
                    GUI.Box(new Rect(5 + cnt * 55, Screen.height - 55, 50, 50), wallTextures[cnt]);
                }
                else if (GUI.Button(new Rect(5 + cnt * 55, Screen.height - 55, 50, 50), wallTextures[cnt]))
                {
                    textureIndex = cnt;
                }
            }

        }
        else if (GUI.Button(new Rect(550, 5, 100, 30), "Wall Textures"))
        {
            editMode = BuildingEditMode.Textures;
        }

        if (editMode == BuildingEditMode.WallTextures)
        {
            GUI.Box(new Rect(660, 5, 100, 30), "Floor Textures");

            if (floorTextureIndex == -1)
            {
                GUI.Box(new Rect(5, 40, 100, 30), "Delete");
            }
            else if (GUI.Button(new Rect(5, 40, 100, 30), "Delete"))
            {
                floorTextureIndex = -1;
            }

            for (int cnt = 0; cnt < floorTextures.Count; cnt++)
            {
                if (floorTextureIndex == cnt)
                {
                    GUI.Box(new Rect(5 + cnt * 55, Screen.height - 55, 50, 50), floorTextures[cnt]);
                }
                else if (GUI.Button(new Rect(5 + cnt * 55, Screen.height - 55, 50, 50), floorTextures[cnt]))
                {
                    floorTextureIndex = cnt;
                }
            }

        }
        else if (GUI.Button(new Rect(660, 5, 100, 30), "Floor Textures"))
        {
            editMode = BuildingEditMode.WallTextures;
        }

        if (editMode == BuildingEditMode.Roofs)
        {
            GUI.Box(new Rect(5, 40, 100, 30), "Roofs");
        }
        else if (GUI.Button(new Rect(5, 40, 100, 30), "Roofs"))
        {
            editMode = BuildingEditMode.Roofs;
        }

        

    }

    float RoundPosition(float pos)
    {

        //if (Mathf.Abs(pos - (int)pos) < .25f)
        //{
        //    return Mathf.RoundToInt(pos);
        //}
        //else if (Mathf.Abs(pos - (int)pos) > .75f)
        //{
        //    return Mathf.RoundToInt(pos);
        //}
        //else
        //{
            return (int)pos + (pos > 0? .5f : -.5f);
        //}
        
    }

    void HandleInput()
    {

        if (Input.GetMouseButton(0))
        {
            if (editMode == BuildingEditMode.Walls || editMode == BuildingEditMode.DestroyWalls)
            {

				HighlightArea(new Vector2(highlightCube.transform.localPosition.x, highlightCube.transform.localPosition.z));
            }
			else if(editMode == BuildingEditMode.Add || editMode == BuildingEditMode.Destroy) {
				HighlightArea(new Vector2(highlightCube.transform.localPosition.x, highlightCube.transform.localPosition.z));

			}
        }
		if (Input.GetMouseButton(0) == false)
		{
			setStarting = false;

			//do something like walls or floors
			if(editMode == BuildingEditMode.Add) {
				for (int cnt = 0; cnt < highlightedGrids.Count; cnt++)
				{
					Vector2 pos = new Vector2(highlightedGrids[cnt].transform.localPosition.x, highlightedGrids[cnt].transform.localPosition.z);
                    if (!floorData[floorLevel].Floors.ContainsKey(pos))
                    {
                        floorData[floorLevel].Floors.Add(pos, new GameObject("Floor").AddComponent<BuildingFloor>());
                        floorData[floorLevel].Floors[pos].transform.parent = building.transform;
                        floorData[floorLevel].Floors[pos].transform.localPosition = new Vector3(pos.x, floorLevel * 2, pos.y);
                        floorData[floorLevel].Floors[pos].transform.localRotation = Quaternion.identity;

                        if (floorLevel > 0)
                        {
                            floorData[floorLevel].Floors[pos].CreateUnderside(undersidePrefab);
                        }

					}

                    floorData[floorLevel].Floors[pos].CreateFloor(floorPrefab);
				}
			}
			else if(editMode == BuildingEditMode.Destroy) {
				for (int cnt = 0; cnt < highlightedGrids.Count; cnt++)
				{
					Vector2 pos = new Vector2(highlightedGrids[cnt].transform.localPosition.x, highlightedGrids[cnt].transform.localPosition.z);
                    if (floorData[floorLevel].Floors.ContainsKey(pos))
                    {
                        Destroy(floorData[floorLevel].Floors[pos].gameObject);
                        floorData[floorLevel].Floors.Remove(pos);
					}

				}
			}
            else if (editMode == BuildingEditMode.Walls)
            {
                for (int cnt = 1; cnt < highlightedGrids.Count; cnt+=1)
                {
                    Vector2 pos1 = new Vector2(highlightedGrids[cnt].transform.localPosition.x, highlightedGrids[cnt].transform.localPosition.z);
                    Vector2 pos2 = new Vector2(highlightedGrids[cnt-1].transform.localPosition.x, highlightedGrids[cnt-1].transform.localPosition.z);

                    Vector2 pos = Vector2.Lerp(pos1, pos2, 0.5f);// (pos2 - pos1) * 0.5f + pos1;


                    if (!floorData[floorLevel].Walls.ContainsKey(pos))
                    {
                        floorData[floorLevel].Walls.Add(pos, new GameObject("Wall").AddComponent<BuildingPiece>());
                        floorData[floorLevel].Walls[pos].transform.parent = building.transform;
                        floorData[floorLevel].Walls[pos].transform.localPosition = new Vector3(pos.x, floorLevel * 2, pos.y);
                        if(pos1.y - pos2.y != 0) {
                            floorData[floorLevel].Walls[pos].transform.localRotation = Quaternion.Euler(0, 90, 0);//highlightedGrids[cnt].transform.localRotation;
                            floorData[floorLevel].Walls[pos].facing = BuildingWallFacing.East;
                        }
                        else {
                            floorData[floorLevel].Walls[pos].transform.localRotation = Quaternion.identity;
                        }

                        floorData[floorLevel].Walls[pos].CreateVisual(wallPrefab);
                    }

                    
                }

                UpdatePosts();
            }
            else if (editMode == BuildingEditMode.DestroyWalls)
            {
                for (int cnt = 1; cnt < highlightedGrids.Count; cnt+=1)
                {
                    Vector2 pos1 = new Vector2(highlightedGrids[cnt].transform.localPosition.x, highlightedGrids[cnt].transform.localPosition.z);
                    Vector2 pos2 = new Vector2(highlightedGrids[cnt-1].transform.localPosition.x, highlightedGrids[cnt-1].transform.localPosition.z);

                    Vector2 pos = Vector2.Lerp(pos1, pos2, 0.5f);// (pos2 - pos1) * 0.5f + pos1;

                    if (floorData[floorLevel].Walls.ContainsKey(pos))
                    {
                        Destroy(floorData[floorLevel].Walls[pos].gameObject);
                        floorData[floorLevel].Walls.Remove(pos);
                    }

                }

                UpdatePosts();
                
            }

			for (int cnt = 0; cnt < highlightedGrids.Count; cnt++)
			{
				Destroy(highlightedGrids[cnt]);
			}
			
			highlightedGrids.Clear();

		}
    }
	
	Vector2 startingHighlightGrid;
	bool setStarting = false;
	Vector2 endHighlightGrid;
	List<GameObject> highlightedGrids = new List<GameObject>();
	public void HighlightArea(Vector2 gridSpace)
	{
		if (setStarting == false)
		{
			//highlightAreas.Clear();
			setStarting = true;
			startingHighlightGrid = gridSpace;
			//highlightAreas.Add(gridSpace);
		}
		else if(endHighlightGrid != gridSpace && gridSpace != startingHighlightGrid)
		{
			endHighlightGrid = gridSpace;
			
			//highlightAreas.Add(gridSpace);
			HighlightArea();
		}
	}
	
	void HighlightArea() {
		
		for (int cnt = 0; cnt < highlightedGrids.Count; cnt++)
		{
			//highlightAreas.Remove(new Vector2(highlightedGrids[cnt].transform.localPosition.x, highlightedGrids[cnt].transform.localPosition.z));
			Destroy(highlightedGrids[cnt]);
		}
		
		highlightedGrids.Clear();

        if (startingHighlightGrid.y < endHighlightGrid.y)
        {
            for (float y = startingHighlightGrid.y; y < endHighlightGrid.y + 1; y++)
            {
                if (startingHighlightGrid.x < endHighlightGrid.x)
                {
                    for (float x = startingHighlightGrid.x; x < endHighlightGrid.x + 1; x++)
                    {


                        highlightedGrids.Add(Instantiate(highlightCube) as GameObject);
                        highlightedGrids[highlightedGrids.Count - 1].transform.parent = highlightCube.transform.parent;
                        highlightedGrids[highlightedGrids.Count - 1].transform.localPosition = new Vector3(x, highlightCube.transform.localPosition.y, y);
                        //highlightAreas.Add(new Vector2(highlightedGrids[highlightedGrids.Count - 1].transform.localPosition.x, highlightedGrids[highlightedGrids.Count - 1].transform.localPosition.z));
                    }
                }
                else
                {
                    for (float x = startingHighlightGrid.x; x > endHighlightGrid.x - 1; x--)
                    {

                        highlightedGrids.Add(Instantiate(highlightCube) as GameObject);
                        highlightedGrids[highlightedGrids.Count - 1].transform.parent = highlightCube.transform.parent;
                        highlightedGrids[highlightedGrids.Count - 1].transform.localPosition = new Vector3(x, highlightCube.transform.localPosition.y, y);
                        //highlightAreas.Add(new Vector2(highlightedGrids[highlightedGrids.Count - 1].transform.localPosition.x, highlightedGrids[highlightedGrids.Count - 1].transform.localPosition.z));
                    }
                }
            }
        }
        else
        {
            for (float y = startingHighlightGrid.y; y > endHighlightGrid.y - 1; y--)
            {
                if (startingHighlightGrid.x > endHighlightGrid.x)
                {
                    for (float x = startingHighlightGrid.x; x > endHighlightGrid.x - 1; x--)
                    {

                        highlightedGrids.Add(Instantiate(highlightCube) as GameObject);
                        highlightedGrids[highlightedGrids.Count - 1].transform.parent = highlightCube.transform.parent;
                        highlightedGrids[highlightedGrids.Count - 1].transform.localPosition = new Vector3(x, highlightCube.transform.localPosition.y, y);
                        //highlightAreas.Add(new Vector2(highlightedGrids[highlightedGrids.Count - 1].transform.localPosition.x, highlightedGrids[highlightedGrids.Count - 1].transform.localPosition.z));
                    }
                }
                else
                {
                    for (float x = startingHighlightGrid.x; x < endHighlightGrid.x + 1; x++)
                    {

                        highlightedGrids.Add(Instantiate(highlightCube) as GameObject);
                        highlightedGrids[highlightedGrids.Count - 1].transform.parent = highlightCube.transform.parent;
                        highlightedGrids[highlightedGrids.Count - 1].transform.localPosition = new Vector3(x, highlightCube.transform.localPosition.y, y);
                        //highlightAreas.Add(new Vector2(highlightedGrids[highlightedGrids.Count - 1].transform.localPosition.x, highlightedGrids[highlightedGrids.Count - 1].transform.localPosition.z));
                    }
                }
            }
        }

	}

    void UpdatePosts()
    {
        foreach (var wallPair in floorData[floorLevel].Walls)
        {
            if (wallPair.Value.facing == BuildingWallFacing.North)
            {
                Vector2 otherPos = wallPair.Key;
                otherPos.x += .5f;
                otherPos.y += .5f;
                bool leftNeeds = false;
                bool rightNeeds = false;
                float rot = 0;
                float rotLeft = 0;


                if (floorData[floorLevel].Walls.ContainsKey(otherPos))
                {
                    rightNeeds = true;
                    rot = 0;
                }

                otherPos.y -= 1.0f;

                if (floorData[floorLevel].Walls.ContainsKey(otherPos))
                {
                    rightNeeds = !rightNeeds;
                    rot = 270;
                }


                otherPos.x -= 1.0f;

                if (floorData[floorLevel].Walls.ContainsKey(otherPos))
                {
                    leftNeeds = true;
                    rotLeft = 180;
                }

                otherPos.y += 1.0f;

                if (floorData[floorLevel].Walls.ContainsKey(otherPos))
                {
                    leftNeeds = !leftNeeds;
                    rotLeft = 90;
                }



                Vector2 postPos = wallPair.Key;
                postPos.x += .5f;


                if (rightNeeds)
                {
                    if (floorData[floorLevel].Posts.ContainsKey(postPos) == false)
                    {

                        floorData[floorLevel].Posts.Add(postPos, Instantiate(postPrefab) as GameObject);
                        floorData[floorLevel].Posts[postPos].transform.parent = building.transform;
                        floorData[floorLevel].Posts[postPos].transform.localRotation = Quaternion.Euler(0, rot, 0);
                        floorData[floorLevel].Posts[postPos].transform.localPosition = new Vector3(postPos.x, floorLevel * 2, postPos.y);
                    }
                }
                else
                {
                    if (floorData[floorLevel].Posts.ContainsKey(postPos) == true)
                    {

                        Destroy(floorData[floorLevel].Posts[postPos]);
                        floorData[floorLevel].Posts.Remove(postPos);
                    }
                }

                postPos.x -= 1.0f;

                if (leftNeeds)
                {
                    if (floorData[floorLevel].Posts.ContainsKey(postPos) == false)
                    {
                        floorData[floorLevel].Posts.Add(postPos, Instantiate(postPrefab) as GameObject);
                        floorData[floorLevel].Posts[postPos].transform.parent = building.transform;
                        floorData[floorLevel].Posts[postPos].transform.localRotation = Quaternion.Euler(0, rotLeft, 0);
                        floorData[floorLevel].Posts[postPos].transform.localPosition = new Vector3(postPos.x, floorLevel * 2, postPos.y);
                    }
                }
                else
                {
                    if (floorData[floorLevel].Posts.ContainsKey(postPos) == true)
                    {

                        Destroy(floorData[floorLevel].Posts[postPos]);
                        floorData[floorLevel].Posts.Remove(postPos);
                    }
                }
            }
            else    //check side walls to make sure there are no left over posts
            {
                Vector2 otherPos = wallPair.Key;
                otherPos.x += .5f;
                otherPos.y += .5f;
                bool forNeeds = false;
                bool backNeeds = false;



                if (floorData[floorLevel].Walls.ContainsKey(otherPos))
                {
                    forNeeds = true;
                   
                }

                otherPos.x -= 1.0f;

                if (floorData[floorLevel].Walls.ContainsKey(otherPos))
                {
                    forNeeds = !forNeeds;
                    
                }


                otherPos.y -= 1.0f;

                if (floorData[floorLevel].Walls.ContainsKey(otherPos))
                {
                    backNeeds = true;
                }

                otherPos.x += 1.0f;

                if (floorData[floorLevel].Walls.ContainsKey(otherPos))
                {
                    backNeeds = !backNeeds;
                }

                Vector2 postPos = wallPair.Key;
                postPos.y += .5f;


                if (!forNeeds)
                {
                    if (floorData[floorLevel].Posts.ContainsKey(postPos) == true)
                    {

                        Destroy(floorData[floorLevel].Posts[postPos]);
                        floorData[floorLevel].Posts.Remove(postPos);
                    }
                }

                postPos.y -= 1.0f;

                if (!backNeeds)
                {
                    if (floorData[floorLevel].Posts.ContainsKey(postPos) == true)
                    {

                        Destroy(floorData[floorLevel].Posts[postPos]);
                        floorData[floorLevel].Posts.Remove(postPos);
                    }
                }

            }
        }
    }


    public GameObject CombineBuilding()
    {

        GameObject newBuilding = TexturePacker.BuildMeshAndTextures(building);
        newBuilding.name = "House1";
        GameObject colliderParent = new GameObject("Colliders");
        colliderParent.transform.parent = newBuilding.transform;
        colliderParent.transform.localPosition = Vector3.zero;
        colliderParent.transform.localRotation = Quaternion.identity;

        Collider[] colliders = building.GetComponentsInChildren<Collider>();

        for (int cnt = 0; cnt < colliders.Length; cnt++)
        {
            if (colliders[cnt].isTrigger)
                continue;

            if (colliders[cnt] is BoxCollider)
            {
                var box = colliders[cnt] as BoxCollider;

                BoxCollider newBox = new GameObject("box").AddComponent<BoxCollider>();
                newBox.transform.position = box.transform.position;
                newBox.transform.rotation = box.transform.rotation;
                newBox.center = box.center;
                newBox.size = box.size;
                newBox.transform.parent = colliderParent.transform;
            }
        }



#if UNITY_EDITOR

        UnityEditor.AssetDatabase.CreateFolder("Assets", saveName);
        UnityEditor.AssetDatabase.CreateFolder("Assets/" + saveName, "Materials");
        UnityEditor.AssetDatabase.SaveAssets();

        MeshFilter[] meshFilters = newBuilding.GetComponentsInChildren<MeshFilter>();
        List<Material> materials = new List<Material>();
        for (int cnt = 0; cnt < meshFilters.Length; cnt++)
        {
            materials.AddRange(meshFilters[cnt].renderer.sharedMaterials);
            UnityEditor.AssetDatabase.CreateAsset(meshFilters[cnt].sharedMesh, "Assets/" + saveName + "/" + "Mesh" + cnt + ".asset");
        }

        // UnityEditor.AssetDatabase.CreateFolder("Assets/House1/", "Materials");
        for (int cnt = 0; cnt < materials.Count; cnt++)
        {
            if (materials[cnt].mainTexture != null)
            {
                UnityEditor.AssetDatabase.CreateAsset(materials[cnt].mainTexture, "Assets/" + saveName + "/Materials/" + saveName + cnt + ".asset");
            }

            UnityEditor.AssetDatabase.CreateAsset(materials[cnt], "Assets/" + saveName + "/Materials/" + saveName + materials[cnt].name + cnt + ".mat");


        }

        UnityEditor.PrefabUtility.CreatePrefab("Assets/" + saveName + "/" + saveName + ".prefab", newBuilding);

        UnityEditor.AssetDatabase.SaveAssets();
#endif
        return newBuilding;

    }
}
