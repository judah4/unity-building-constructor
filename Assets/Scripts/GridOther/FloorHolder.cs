﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FloorHolder {

    public  Dictionary<Vector2, BuildingFloor> Floors { get; private set; }
    public  Dictionary<Vector2, BuildingPiece> Walls  { get; private set; }
    public  Dictionary<Vector2, GameObject> Posts { get; private set; }

	// Use this for initialization
	public FloorHolder () {
        Floors = new Dictionary<Vector2, BuildingFloor>();
        Walls = new Dictionary<Vector2, BuildingPiece>();
        Posts = new Dictionary<Vector2, GameObject>();
	}
	
}
