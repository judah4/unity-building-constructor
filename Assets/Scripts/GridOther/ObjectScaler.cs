﻿using UnityEngine;
using System.Collections;

public class ObjectScaler : MonoBehaviour {

    public GameObject draggerNorth;
    public GameObject draggerSouth;
    public GameObject draggerEast;
    public GameObject draggerWest;
    public GameObject draggerHolder;
    public GameObject dragCollider;

    bool dragNorth = false;
    bool dragSouth = false;
    bool dragEast = false;
    bool dragWest = false;

    bool hoverOver = false;
    void Awake()
    {
        draggerHolder = new GameObject("Draggers");
        draggerHolder.transform.position = transform.position;
        draggerHolder.transform.rotation = transform.rotation;
        transform.parent = draggerHolder.transform;
    }

	// Use this for initialization
	void Start () {

        draggerNorth = GameObject.CreatePrimitive(PrimitiveType.Cube);
        draggerNorth.transform.parent = draggerHolder.transform;
        draggerNorth.transform.localPosition = new Vector3(0, 0, 1);
        draggerNorth.transform.localRotation = Quaternion.identity;
        draggerNorth.layer = 18;

        draggerSouth = GameObject.CreatePrimitive(PrimitiveType.Cube);
        draggerSouth.transform.parent = draggerHolder.transform;
        draggerSouth.transform.localPosition = new Vector3(0, 0, -1);
        draggerSouth.transform.localRotation = Quaternion.identity;
        draggerSouth.layer = 18;

        draggerEast = GameObject.CreatePrimitive(PrimitiveType.Cube);
        draggerEast.transform.parent = draggerHolder.transform;
        draggerEast.transform.localPosition = new Vector3(1, 0, 0);
        draggerEast.transform.localRotation = Quaternion.identity;
        draggerEast.layer = 18;

        draggerWest = GameObject.CreatePrimitive(PrimitiveType.Cube);
        draggerWest.transform.parent = draggerHolder.transform;
        draggerWest.transform.localPosition = new Vector3(-1, 0, 0);
        draggerWest.transform.localRotation = Quaternion.identity;
        draggerWest.layer = 18;

        dragCollider = GameObject.CreatePrimitive(PrimitiveType.Cube);
        dragCollider.transform.parent = draggerHolder.transform;
        dragCollider.transform.localPosition = new Vector3(0, 0, 0);
        dragCollider.transform.localRotation = Quaternion.identity;
        dragCollider.layer = 19;
        Destroy(dragCollider.renderer);

	}
	
	// Update is called once per frame
	void Update () {
        //draggerHolder.transform.rotation = transform.rotation;

	    Ray ray = Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.nearClipPlane));//new Ray(Camera.main.ScreenPointToRay(Input.mousePosition), Camera.main.transform.forward);
        RaycastHit info;

        if (Physics.Raycast(ray, out info, 200, 1 << 18))
        {

            hoverOver = true;
            if (Input.GetMouseButton(0) == true && NothingDragged())
            {

                if (info.collider.gameObject == draggerNorth)
                {
                    dragNorth = true;
                    //draggerNorth.transform.localPosition = new Vector3(0, 0, pos.z);//(int)pos.z + (pos.z > 0 ? .5f : -.5f));
                }

                if (info.collider.gameObject == draggerSouth)
                {
                    dragSouth= true;
                    //draggerNorth.transform.localPosition = new Vector3(0, 0, pos.z);//(int)pos.z + (pos.z > 0 ? .5f : -.5f));
                }

                if (info.collider.gameObject == draggerEast)
                {
                    dragEast = true;
                    //draggerEast.transform.localPosition = new Vector3(pos.x, 0, 0);//(int)pos.z + (pos.z > 0 ? .5f : -.5f));
                }

                if (info.collider.gameObject == draggerWest)
                {
                    dragWest = true;
                    //draggerWest.transform.localPosition = new Vector3(pos.x, 0, 0);//(int)pos.z + (pos.z > 0 ? .5f : -.5f));
                }
            }
  
            
        }

        
        if (dragNorth)
        {
            dragCollider.transform.position = draggerNorth.transform.position;
            dragCollider.transform.localScale = new Vector3(100, .5f, 100);
            ray = Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.nearClipPlane));
            if (Physics.Raycast(ray, out info, 200, 1 << 19))
            {
                Vector3 pos = draggerHolder.transform.InverseTransformPoint(info.point);
                draggerNorth.transform.localPosition = new Vector3(0, 0,Mathf.RoundToInt(pos.z) );//(int)pos.z + (pos.z > 0 ? .5f : -.5f));
            }
        }

        if (dragSouth)
        {
            dragCollider.transform.position = draggerSouth.transform.position;
            dragCollider.transform.localScale = new Vector3(100, .5f, 100);
            ray = Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.nearClipPlane));
            if (Physics.Raycast(ray, out info, 200, 1 << 19))
            {
                Vector3 pos = draggerHolder.transform.InverseTransformPoint(info.point);

                draggerSouth.transform.localPosition = new Vector3(0, 0, Mathf.RoundToInt(pos.z));//(int)pos.z + (pos.z > 0 ? .5f : -.5f));

            }
        }

        if (dragEast)
        {
            dragCollider.transform.position = draggerEast.transform.position;
            dragCollider.transform.localScale = new Vector3(100, .5f, 100);
            ray = Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.nearClipPlane));
            if (Physics.Raycast(ray, out info, 200, 1 << 19))
            {
                Vector3 pos = draggerHolder.transform.InverseTransformPoint(info.point);

                draggerEast.transform.localPosition = new Vector3(Mathf.RoundToInt(pos.x), 0, 0);//(int)pos.z + (pos.z > 0 ? .5f : -.5f));
            }
        }

        if (dragWest)
        {
            dragCollider.transform.position = draggerWest.transform.position;
            dragCollider.transform.localScale = new Vector3(100, .5f, 100);
            ray = Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.nearClipPlane));
            if (Physics.Raycast(ray, out info, 200, 1 << 19))
            {
                Vector3 pos = draggerHolder.transform.InverseTransformPoint(info.point);

                draggerWest.transform.localPosition = new Vector3(Mathf.RoundToInt(pos.x), 0, 0);//(int)pos.z + (pos.z > 0 ? .5f : -.5f));
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            dragNorth = false;
            dragSouth = false;
            dragEast = false;
            dragWest = false;
        }


        HoverOverHandling();
        ResizeRoof();
	}

    void HoverOverHandling()
    {
        if (hoverOver)
        {
            draggerNorth.renderer.material.color = Color.yellow;
            draggerSouth.renderer.material.color = Color.yellow;
            draggerEast.renderer.material.color = Color.yellow;
            draggerWest.renderer.material.color = Color.yellow;

            if (NothingDragged())
            {
                hoverOver = false;
            }
        }
        else
        {
            draggerNorth.renderer.material.color = Color.white;
            draggerSouth.renderer.material.color = Color.white;
            draggerEast.renderer.material.color = Color.white;
            draggerWest.renderer.material.color = Color.white;
        }

    }

    bool NothingDragged()
    {
        return dragNorth == false && dragSouth == false && dragWest == false && dragEast == false;
    }

    void ResizeRoof()
    {
        float xDist = Vector3.Distance(draggerEast.transform.localPosition, draggerWest.transform.localPosition);
        float zDist = Vector3.Distance(draggerNorth.transform.localPosition , draggerSouth.transform.localPosition);

        float xOff = Vector3.Lerp(draggerEast.transform.localPosition, draggerWest.transform.localPosition, .5f).x; //draggerWest.transform.localPosition.x + draggerEast.transform.localPosition.x;
        float zOff = Vector3.Lerp(draggerNorth.transform.localPosition, draggerSouth.transform.localPosition, .5f).z;//draggerSouth.transform.localPosition.z + draggerNorth.transform.localPosition.z;

        //tile.roof.transform.localScale = new Vector3(tile.roof.transform.localScale.x + .75f, tile.roof.transform.localScale.y, tile.roof.transform.localScale.z);
        //tile.roof.transform.localPosition = new Vector3(tile.roof.transform.localPosition.x + .5f, tile.roof.transform.localPosition.y, tile.roof.transform.localPosition.z);

        transform.localScale = new Vector3(xDist * .77f - .5f, xDist * .25f, zDist - 1f);
        transform.localPosition = new Vector3(xOff, transform.localPosition.y, zOff);

        draggerNorth.transform.localPosition = new Vector3(transform.localPosition.x, draggerNorth.transform.localPosition.y, draggerNorth.transform.localPosition.z);
        draggerSouth.transform.localPosition = new Vector3(transform.localPosition.x, draggerSouth.transform.localPosition.y, draggerSouth.transform.localPosition.z);

        draggerEast.transform.localPosition = new Vector3(draggerEast.transform.localPosition.x, draggerEast.transform.localPosition.y, transform.localPosition.z);
        draggerWest.transform.localPosition = new Vector3(draggerWest.transform.localPosition.x, draggerWest.transform.localPosition.y, transform.localPosition.z);
    

    }

    void OnDestroy()
    {
#if UNITY_EDITOR
        if (Application.isPlaying && draggerHolder != null)
        {
            transform.parent = null;
            Destroy(draggerHolder);
        }
#endif
    }
}
