﻿using UnityEngine;
using System.Collections;

public class BuildingRoof : MonoBehaviour {

    public new Renderer renderer;
	public GameObject roofPrefab;
	public GameObject roof;
    public Texture texture;
    Texture startTexture;
    GameObject underside;

    bool hoverOver = true;
    Color startColor;
    [HideInInspector]
    public ObjectScaler objectScaler;

    void Awake()
    {
        objectScaler = new GameObject("objectScaler").AddComponent<ObjectScaler>();
        objectScaler.gameObject.SetActive(false);
    }

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        if (renderer != null)
        {
            if (hoverOver)
            {
                renderer.material.color = Color.yellow;
            }
            else
            {
                renderer.material.color = startColor;
            }
        }
        hoverOver = false;
	}

    public void HoverOver()
    {

        hoverOver = true;


    }

    


	public void CreateRoof(GameObject prefab) {
		if(prefab !=roofPrefab) {
            roofPrefab = prefab;
			if(roof != null)
				Destroy(roof);
		}

        if (roof == null)
        {
            roof = Instantiate(roofPrefab) as GameObject;
            roof.transform.parent = transform;
            roof.transform.localPosition = Vector3.zero;
            roof.transform.localRotation = Quaternion.identity;
            roof.layer = 17;

            renderer = GetComponentInChildren<Renderer>();
            startColor = renderer.material.color;
            startTexture = renderer.material.mainTexture;
            if (texture != null)
            {
                renderer.material.mainTexture = texture;
            }
		}
    }

    public void ChangeTexture(Texture texture)
    {
            
        this.texture = texture;

        if (texture != null)
        {
            renderer.material.mainTexture = texture;
        }
        else
        {
            renderer.material.mainTexture = startTexture;
        }

    }

    public void CreateUnderside(GameObject prefab)
    {

        if (underside == null)
        {
            underside = Instantiate(prefab) as GameObject;
            underside.transform.parent = transform;
            underside.transform.localPosition = new Vector3(0, -.05f, 0);
            underside.transform.localRotation = Quaternion.Euler(0, 0, 180);
            
        }
    }
	
}
