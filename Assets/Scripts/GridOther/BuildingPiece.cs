﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BuildingPiece : MonoBehaviour {

	public GameObject prefab;
    public BuildingPieceSettings visual;
    public BuildingPieceSettings visualBack;
    public Texture texture;
    public Texture textureBack;


    public BuildingWallFacing facing = BuildingWallFacing.North;

    bool hoverOver = false;
    bool hoverOverBack = false;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (hoverOver)
        {
            for (int cnt = 0; cnt < visual.renderers.Count; cnt++)
            {
                visual.renderers[cnt].material.color = Color.yellow;
            }

        }
        else
        {
            if (visual != null)
            {
                for (int cnt = 0; cnt < visual.renderers.Count; cnt++)
                {
                    visual.renderers[cnt].material.color = Color.white;
                }
            }
        }

        if (hoverOverBack)
        {
            for (int cnt = 0; cnt < visualBack.renderers.Count; cnt++)
            {
                visualBack.renderers[cnt].material.color = Color.yellow;
            }

        }
        else
        {
            if (visualBack != null)
            {
                for (int cnt = 0; cnt < visualBack.renderers.Count; cnt++)
                {
                    visualBack.renderers[cnt].material.color = Color.white;
                }
            }
        }


        hoverOver = false;
        hoverOverBack = false;
    }

    public void HoverOver(GameObject hoverOvered)
    {


        if (hoverOvered == visual.gameObject && visual != null)
        {
            hoverOver = true;
        }

        if (hoverOvered == visualBack.gameObject && visualBack != null)
        {
            hoverOverBack = true;
        }

    }

	public void CreateVisual(GameObject prefab) {
		if(prefab != this.prefab) {
			this.prefab = prefab;
            if (visual != null)
            {
                Destroy(visual.gameObject);
                Destroy(visualBack.gameObject);
                visual = null;
                visualBack = null;
            }
		}

        if (visual == null)
        {
            var gm = Instantiate(prefab) as GameObject;
            visual = gm.GetComponent<BuildingPieceSettings>();
            if (visual == null)
                visual = gm.AddComponent<BuildingPieceSettings>();
            visual.transform.parent = transform;
            visual.transform.localPosition = new Vector3(0, 0, .14f);
            visual.transform.localRotation = Quaternion.Euler(0, 180, 0);
            visual.gameObject.layer = 15;

            gm = Instantiate(prefab) as GameObject;
            visualBack = gm.GetComponent<BuildingPieceSettings>();
            if(visualBack == null)
                visualBack = gm.AddComponent<BuildingPieceSettings>();
            visualBack.transform.parent = transform;
            visualBack.transform.localPosition = new Vector3(0, 0, -.14f);
            visualBack.transform.localRotation = Quaternion.identity;
            visualBack.gameObject.layer = 15;

            UpdateTextures();
		}
	}

    void UpdateTextures()
    {
        for (int cnt = 0; cnt < visual.renderers.Count; cnt++)
        {
            if (texture != null && visual.renderers[cnt].material.mainTexture != texture)
            {
                visual.renderers[cnt].renderer.material.mainTexture = texture;
            }
        }
        for (int cnt = 0; cnt < visualBack.renderers.Count; cnt++)
        {
            if (textureBack != null && visualBack.renderers[cnt].material.mainTexture != textureBack)
            {
                visualBack.renderers[cnt].material.mainTexture = textureBack;
            }
        }
    }

    public void ChangeTexture(Texture texture, GameObject clickedObject)
    {
        if (clickedObject == visual.gameObject)
        {
            this.texture = texture;
        }

        if (clickedObject == visualBack.gameObject)
        {
            textureBack = texture;
        }

        UpdateTextures();
    }
}
