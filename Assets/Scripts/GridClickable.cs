﻿using UnityEngine;
using System.Collections;

public class GridClickable : MonoBehaviour {

    bool hoverOver = false;

    public bool dragging = false;

    [HideInInspector]
    public int x;
    [HideInInspector]
    public int y;

    public new Renderer renderer;
    
    public BuildingGrid buildingGrid;
    public BuildingTile tile;

    public GridClickable northTile;
    public GridClickable eastTile;
    public GridClickable southTile;
    public GridClickable westTile;

    

	// Use this for initialization
	void Start () {
		gameObject.layer = 14;
	}
	
	// Update is called once per frame
	void Update () {

        if (buildingGrid.hideGrid)
        {
            renderer.enabled = false;
            collider.enabled = false;
        }
        else
        {


            renderer.enabled = true;
            collider.enabled = true;
            if (hoverOver || dragging)
            {
                renderer.material.color = Color.yellow;
            }
            else
            {
                renderer.material.color = Color.white;

            }


            if (dragging)
            {
                if (!Input.GetMouseButton(0))
                {
                    buildingGrid.ChangeTile(this);
                    dragging = false;
                }
            }

        }
        hoverOver = false;
        
	}



    public void HandleInput()
    {

        hoverOver = true;

        if (Input.GetMouseButton(0))
        {
            buildingGrid.HighlightArea(this);
            
        }
		
    }

    public void ChangeWallTexture(Texture wallTexture, BuildingWallFacing facing, GameObject clickedWall)
    {
        hoverOver = true;

        if (Input.GetMouseButton(0) && tile != null)
        {
            if (Input.GetKey(KeyCode.LeftShift))
            {
                tile.RemoveWallTextureIfSame(wallTexture, facing);
            }
            else
            {
                tile.ChangeWallTexture(wallTexture, facing, clickedWall);
            }
            buildingGrid.wallsDirty = true;

        }
    }

    public void ChangeWallType(GameObject wallPrefab, BuildingWallFacing facing, GameObject clickedWall) {
        hoverOver = true;

        if (Input.GetMouseButtonDown(0) && tile != null)
        {
            if (Input.GetKey(KeyCode.LeftShift))
            {
                tile.RemoveWallPrefabIfSame(wallPrefab, facing);
            }
            else
            {
                tile.ChangeWallPrefab(wallPrefab, facing, clickedWall);
            }
            buildingGrid.wallsDirty = true;
            
        }
    }

    public bool NorthTileExists()
    {
        return northTile != null && northTile.tile != null;
    }

    public bool EastTileExists()
    {
        return eastTile != null && eastTile.tile != null;
    }

    public bool SouthTileExists()
    {
        return southTile != null && southTile.tile != null;
    }

    public bool WestTileExists()
    {
        return westTile != null && westTile.tile != null;
    }

}
