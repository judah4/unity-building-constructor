﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BuildingWall : MonoBehaviour {

    public GameObject outsideWall;
    public GameObject insideWall;


    public List<Renderer> outRenderers = new List<Renderer>();
    public List<Renderer> inRenderers = new List<Renderer>();

    public Texture inTexture;
    public Texture outTexture;

    public GameObject altWallOutPrefab;
    public GameObject altWallInPrefab;
    

   // public GridClickable gridSpace;
    public BuildingTile tile;
    public BuildingWallFacing facing = BuildingWallFacing.North;

    bool hoverOverOut = false;
    bool hoverOverIn = false;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        if (hoverOverOut)
        {
            for (int cnt = 0; cnt < outRenderers.Count; cnt++)
            {
                outRenderers[cnt].material.color = Color.yellow;
            }

        }
        else
        {
            for (int cnt = 0; cnt < outRenderers.Count; cnt++)
            {
                outRenderers[cnt].material.color = Color.white;
            }

        }

        if (hoverOverIn)
        {
            for (int cnt = 0; cnt < inRenderers.Count; cnt++)
            {
                inRenderers[cnt].material.color = Color.yellow;
            }

        }
        else
        {
            for (int cnt = 0; cnt < inRenderers.Count; cnt++)
            {
                inRenderers[cnt].material.color = Color.white;
            }

        }


        hoverOverOut = false;
        hoverOverIn = false;
	}

    public void HoverOver(GameObject hoverOvered)
    {
        

        if (hoverOvered == outsideWall)
        {
            hoverOverOut = true;
        }

        if (hoverOvered == insideWall)
        {
            hoverOverIn = true;
        }

    }
    public void CreateWall(GameObject prefab)
    {
        GameObject prefabToUse = prefab;

        if (outsideWall != null)
        {
            outRenderers.Clear();
            DestroyImmediate(outsideWall);
        }
        

        if (altWallOutPrefab != null)
            prefabToUse = altWallOutPrefab;

        outsideWall = Instantiate(prefabToUse) as GameObject;
        outsideWall.transform.parent = transform;
        outsideWall.gameObject.layer = gameObject.layer;
        outsideWall.transform.localPosition = new Vector3(0, 0, -.2f);
        outsideWall.transform.localRotation = Quaternion.Euler(0, 0, 0);


        outRenderers = new List<Renderer>(outsideWall.GetComponentsInChildren<Renderer>());

        if (outTexture != null)
        {
            outRenderers[0].material.mainTexture = outTexture;
        }


        if (insideWall != null)
        {
            inRenderers.Clear();
            DestroyImmediate(insideWall);
        }

        prefabToUse = prefab;

        if (altWallInPrefab != null)
            prefab = altWallInPrefab;

        insideWall = Instantiate(prefab) as GameObject;
        insideWall.transform.parent = transform;
        insideWall.gameObject.layer = gameObject.layer;
        insideWall.transform.localPosition = new Vector3(0, 0, 0);
        insideWall.transform.localRotation = Quaternion.Euler(0, 180, 0);

        inRenderers = new List<Renderer>(insideWall.GetComponentsInChildren<Renderer>());

        if (inTexture != null)
        {
            inRenderers[0].material.mainTexture = inTexture;
        }

    }

    public void RemoveWalls()
    {
        if (outsideWall != null)
        {
            outRenderers.Clear();
            DestroyImmediate(outsideWall);
        }

        if (insideWall != null)
        {
            inRenderers.Clear();
            DestroyImmediate(insideWall);
        }
    }

    public void ChangeWallType(GameObject wallPiece, GameObject clickedWall)
    {
        if (clickedWall == insideWall || clickedWall == null || (wallPiece != null && wallPiece.tag == "BothSides"))
        {
            altWallInPrefab = wallPiece;
        }

        if (clickedWall == outsideWall || clickedWall == null || (wallPiece != null && wallPiece.tag == "BothSides"))
        {
            altWallOutPrefab = wallPiece;
        }
    }

    public void ChangeWallTexture(Texture wallTexture, GameObject clickedWall)
    {
        if (clickedWall == insideWall || wallTexture == null)
        {
            inTexture = wallTexture;
        }

        if (clickedWall == outsideWall || wallTexture == null)
        {
            outTexture = wallTexture;
        }
    }

    public void ChangeWallType(GameObject wallPiece)
    {
        altWallOutPrefab = wallPiece;
        altWallInPrefab = wallPiece;
    }

    public void RemoveWallPrefabIfSame(GameObject wallPrefab)
    {

        if (wallPrefab == altWallOutPrefab)
        {
            altWallOutPrefab = null;
        }
        if (wallPrefab == altWallInPrefab)
        {
            altWallInPrefab = null;
        }
    }

    public void RemoveWallTextureIfSame(Texture wallTexture)
    {

        if (wallTexture == outTexture)
        {
            outTexture = null;
        }
        if (wallTexture == inTexture)
        {
            inTexture = null;
        }
    }
}
