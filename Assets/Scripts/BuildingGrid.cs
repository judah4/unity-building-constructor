﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BuildingGrid : MonoBehaviour {

    
    public GridClickable gridPrefab;
    public BuildingTile tilePrefab;
    public GameObject wallPrefab;
    public GameObject postPrefab;
	public GameObject RoofPrefab;
	public GameObject building;
    public List<GameObject> doorPrefabs = new List<GameObject>();
    public List<Texture> wallTextures = new List<Texture>();

    public BuildingEditMode editMode = BuildingEditMode.Add;
    public bool hideGrid = false;
    public int doorIndex = 0;
    public int textureIndex = 0;
    public string saveName = "House1";
    
    List<GridClickable> grid = new List<GridClickable>();
    List<BuildingTile> tiles = new List<BuildingTile>();
    List<BuildingWall> walls = new List<BuildingWall>();
    List<GameObject> posts = new List<GameObject>();

    

	//List<GameObject> roofs = new List<GameObject>();


    int gridSize = 100;
    [HideInInspector]
    public bool wallsDirty = false;

	// Use this for initialization
	void Start () {
		building = new GameObject("Building");
		building.transform.parent = transform;
		building.transform.localPosition = Vector3.zero;
		building.transform.localRotation = Quaternion.identity;

        for (int y = 0; y < gridSize; y++)
        {
            for (int x = 0; x < gridSize; x++)
            {
                grid.Add((GameObject.Instantiate(gridPrefab) as GridClickable));//.GetComponent<GridClickable>());
                grid[grid.Count - 1].transform.parent = transform;
                grid[grid.Count - 1].transform.localPosition = new Vector3(x - gridSize/2, 0, y - gridSize/2);
                grid[grid.Count - 1].transform.localRotation = Quaternion.identity;

                grid[grid.Count - 1].buildingGrid = this;
                grid[grid.Count - 1].x = x;
                grid[grid.Count - 1].y = y;

                if (y > 0)
                {
                    grid[grid.Count - 1].southTile = grid[x + gridSize * (y - 1)];
                    grid[x + gridSize * (y - 1)].northTile = grid[grid.Count - 1];
                }

                if (x > 0)
                {
                    grid[grid.Count - 1].westTile = grid[(x - 1) + gridSize * y];
                    grid[(x - 1) + gridSize * y].eastTile = grid[grid.Count - 1];
                }

            }
        }
	}

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F2))
        {
            Application.LoadLevel(1);
        }

        if (Input.GetMouseButton(0) == false)
        {
            startingHighlightGrid = null;
            endHighlightGrid = null;
        }
		if (Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.RightShift))
		{
			if(editMode == BuildingEditMode.Add) {
				editMode = BuildingEditMode.Destroy;
			}
			else if(editMode == BuildingEditMode.Destroy) {
				editMode = BuildingEditMode.Add;
			}
		}

		if (Input.GetKeyUp(KeyCode.LeftShift) || Input.GetKeyUp(KeyCode.RightShift))
		{
			if(editMode == BuildingEditMode.Add) {
				editMode = BuildingEditMode.Destroy;
			}
			else if(editMode == BuildingEditMode.Destroy) {
				editMode = BuildingEditMode.Add;
			}
		}
			
			Ray ray = Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.nearClipPlane));//new Ray(Camera.main.ScreenPointToRay(Input.mousePosition), Camera.main.transform.forward);
		RaycastHit info;
        if (editMode == BuildingEditMode.Doors)
        {
            if (Physics.Raycast(ray, out info, 200, 1 << 15))
            {

                BuildingWall wallClick = info.collider.transform.parent.GetComponent<BuildingWall>();

                if (wallClick != null)
                {
                    if(doorIndex == -1)
                        wallClick.tile.gridSpace.ChangeWallType(null, wallClick.facing, info.collider.gameObject);
                    else
                        wallClick.tile.gridSpace.ChangeWallType(doorPrefabs[doorIndex], wallClick.facing, info.collider.gameObject);
                    wallClick.HoverOver(info.collider.gameObject);
                }
            }
        }
        else if (editMode == BuildingEditMode.Textures)
        {
            if (Physics.Raycast(ray, out info, 200, 1 << 15))
            {

                BuildingWall wallClick = info.collider.transform.parent.GetComponent<BuildingWall>();

                if (wallClick != null)
                {
                    if (textureIndex == -1)
                        wallClick.tile.gridSpace.ChangeWallTexture(null, wallClick.facing, info.collider.gameObject);
                    else
                        wallClick.tile.gridSpace.ChangeWallTexture(wallTextures[textureIndex], wallClick.facing, info.collider.gameObject);
                    wallClick.HoverOver(info.collider.gameObject);
                }
            }
        }
        else
        {
            if (Physics.Raycast(ray, out info, 200, 1 << 14))
            {
                GridClickable gridClick = info.collider.GetComponent<GridClickable>();
                if (gridClick != null)
                {
                    gridClick.HandleInput();
                    
                }
            }
        }

        if (wallsDirty)
        {
            //create walls
            
            UpdateWalls();
            wallsDirty = false;
        }
	}

    void OnGUI()
    {
		hideGrid = GUI.Toggle(new Rect(Screen.width - 105, 5, 100, 30), hideGrid, "Hide Grid");

        if (GUI.Button(new Rect(Screen.width - 210, 5, 100, 30), "CombineMesh"))
        {
            CombineBuilding();
        }

        if (editMode == BuildingEditMode.Add)
        {
            GUI.Box(new Rect(5, 5, 100, 30), "Create");
        }
        else if (GUI.Button(new Rect(5, 5, 100, 30), "Create"))
        {
            editMode = BuildingEditMode.Add;
        }
        
        if (editMode == BuildingEditMode.Destroy)
        {
            GUI.Box(new Rect(110, 5, 100, 30), "Destroy");
        }
        else if (GUI.Button(new Rect(110, 5, 100, 30), "Destroy"))
        {
            editMode = BuildingEditMode.Destroy;
        }

        if (editMode == BuildingEditMode.Doors)
        {
            GUI.Box(new Rect(220, 5, 100, 30), "Doors");

            if (doorIndex == -1)
            {
                GUI.Box(new Rect(5, 40, 100, 30), "Delete");
            }
            else if (GUI.Button(new Rect(5, 40, 100, 30), "Delete"))
            {
                doorIndex = -1;
            }

			for(int cnt = 0; cnt < doorPrefabs.Count; cnt++) {
				if(doorIndex == cnt) {
					GUI.Box(new Rect(5, 75 + 35 * cnt, 100, 30), doorPrefabs[cnt].name);
				}
                else if (GUI.Button(new Rect(5, 75 + 35 * cnt, 100, 30), doorPrefabs[cnt].name))
                {
					doorIndex = cnt;
				}
			}

        }
        else if (GUI.Button(new Rect(220, 5, 100, 30), "Doors"))
        {
            editMode = BuildingEditMode.Doors;
        }

        if (editMode == BuildingEditMode.Textures)
        {
            GUI.Box(new Rect(330, 5, 100, 30), "Textures");

            if (textureIndex == -1)
            {
                GUI.Box(new Rect(5, 40, 100, 30), "Delete");
            }
            else if (GUI.Button(new Rect(5, 40, 100, 30), "Delete"))
            {
                textureIndex = -1;
            }

            for (int cnt = 0; cnt < wallTextures.Count; cnt++)
            {
                if (textureIndex == cnt)
                {
                    GUI.Box(new Rect(5 + cnt * 55, Screen.height - 55, 50, 50), wallTextures[cnt]);
                }
                else if (GUI.Button(new Rect(5 + cnt * 55, Screen.height - 55, 50, 50), wallTextures[cnt]))
                {
                    textureIndex = cnt;
                }
            }

        }
        else if (GUI.Button(new Rect(330, 5, 100, 30), "Textures"))
        {
            editMode = BuildingEditMode.Textures;
        }     
        
        
    }

    public void ChangeTile(GridClickable gridSpace)
    {
        if (editMode == BuildingEditMode.Add)
        {
            if (gridSpace.tile == null)
            {
                gridSpace.tile = Instantiate(tilePrefab) as BuildingTile;
                gridSpace.tile.gridSpace = gridSpace;
                gridSpace.tile.transform.parent = building.transform;
				gridSpace.tile.transform.position = gridSpace.transform.position;
                gridSpace.tile.transform.localRotation = Quaternion.identity;
                tiles.Add(gridSpace.tile);

                wallsDirty = true;
            }
        }
        else if(editMode == BuildingEditMode.Destroy)
        {
            if (gridSpace.tile != null)
            {
                tiles.Remove(gridSpace.tile);
                Destroy(gridSpace.tile.gameObject);

                wallsDirty = true;
            }
        }
    }

    GridClickable startingHighlightGrid;
    GridClickable endHighlightGrid;
    List<GridClickable> highlightedGrids = new List<GridClickable>();
    public void HighlightArea(GridClickable gridSpace)
    {
        if (startingHighlightGrid == null)
        {
            startingHighlightGrid = gridSpace;
        }
        else if(endHighlightGrid != gridSpace)
        {
            endHighlightGrid = gridSpace;


            HighlightArea();
        }
    }

    void HighlightArea() {

        for (int cnt = 0; cnt < highlightedGrids.Count; cnt++)
        {
            highlightedGrids[cnt].dragging = false;
        }

        highlightedGrids.Clear();

        for(int y = startingHighlightGrid.y; y < endHighlightGrid.y + 1; y++) {
            for(int x = startingHighlightGrid.x; x < endHighlightGrid.x + 1; x++) {
                grid[x + gridSize * y].dragging = true;
                highlightedGrids.Add(grid[x + gridSize * y]);
            }

            for (int x = startingHighlightGrid.x; x > endHighlightGrid.x - 1; x--)
            {
                grid[x + gridSize * y].dragging = true;
                highlightedGrids.Add(grid[x + gridSize * y]);
            }
        }

        for (int y = startingHighlightGrid.y; y > endHighlightGrid.y - 1; y--)
        {
            for (int x = startingHighlightGrid.x; x > endHighlightGrid.x - 1; x--)
            {
                grid[x + gridSize * y].dragging = true;
                highlightedGrids.Add(grid[x + gridSize * y]);
            }

            for (int x = startingHighlightGrid.x; x < endHighlightGrid.x + 1; x++)
            {
                grid[x + gridSize * y].dragging = true;
                highlightedGrids.Add(grid[x + gridSize * y]);
            }
        }
    }

    public void UpdateWalls()
    {

        for (int cnt = 0; cnt < tiles.Count; cnt++)
        {
            BuildingWall wall = null;
            bool northE = true;
            bool eastE = true;
            bool southE = true;
            bool westE = true;

            if (!tiles[cnt].gridSpace.NorthTileExists())
            {
                northE = false;
                wall = tiles[cnt].CreateWall(wallPrefab, BuildingWallFacing.North);
                
            }
            else
            {
                var rwall = tiles[cnt].RemoveWall(BuildingWallFacing.North);
                walls.Remove(rwall);

            }
            if (!tiles[cnt].gridSpace.EastTileExists())
            {
                eastE = false;
                wall = tiles[cnt].CreateWall(wallPrefab, BuildingWallFacing.East);
                
            }
            else
            {
                var rwall = tiles[cnt].RemoveWall(BuildingWallFacing.East);
                walls.Remove(rwall);

            }
            if (!tiles[cnt].gridSpace.SouthTileExists())
            {
                southE = false;
                wall = tiles[cnt].CreateWall(wallPrefab, BuildingWallFacing.South);
            }
            else
            {
                var rwall = tiles[cnt].RemoveWall(BuildingWallFacing.South);
                walls.Remove(rwall);

            }
            if (!tiles[cnt].gridSpace.WestTileExists())
            {
                westE = false;
                wall = tiles[cnt].CreateWall(wallPrefab, BuildingWallFacing.West);
            }
            else
            {
                var rwall = tiles[cnt].RemoveWall(BuildingWallFacing.West);
                walls.Remove(rwall);

            }

            if (wall != null && walls.Contains(wall) == false)
            {
                walls.Add(wall);
            }

            UpdatePost(tiles[cnt], northE, southE, eastE, westE);

        }

        

        //for (int cnt = 0; cnt < roofs.Count; cnt++) {
        //    Destroy(roofs[cnt]);
        //}
        //roofs.Clear();

        //for (int y = 0; y < gridSize; y++)
        //{
        //    GameObject previousRoof = null;

        //    for (int x = 0; x < gridSize; x++)
        //    {

        //        var tile = grid[x + gridSize * y].tile;

        //        if(tile != null) { 
        //            if(tile.roof == null) {
        //                if(previousRoof != null) {
        //                    tile.roof = previousRoof;
        //                    tile.roof.transform.localScale = new Vector3(tile.roof.transform.localScale.x + .75f, tile.roof.transform.localScale.y, tile.roof.transform.localScale.z);
        //                    tile.roof.transform.localPosition = new Vector3(tile.roof.transform.localPosition.x + .5f, tile.roof.transform.localPosition.y, tile.roof.transform.localPosition.z);
        //                }
        //                else {
        //                    tile.roof =  Instantiate(RoofPrefab) as GameObject;
        //                    tile.roof.transform.parent = building.transform;
        //                    tile.roof.transform.position = new Vector3(tile.transform.position.x, tile.transform.position.y + 2, tile.transform.position.z);
        //                    roofs.Add(tile.roof);
        //                    previousRoof = tile.roof;
        //                }
        //            }
        //        }
        //        else {
        //            previousRoof = null;
        //        }
        //    }
        //}

	}

    public void RemoveWall(BuildingWall wall)
    {
        walls.Remove(wall);
    }
		
	void UpdatePost(BuildingTile tile, bool northExists, bool southExists, bool eastExists, bool westExists)
    {

        if (!northExists)
        {
            if (!eastExists)
            {
                var post = tile.CreatePost(postPrefab, BuildingWallFacing.NorthEast);
                if (post != null)
                {
                    posts.Add(post);
                }
            }
            else
            {
                var rwall = tile.RemovePost(BuildingWallFacing.NorthEast);
                posts.Remove(rwall);
                if (rwall != null)
                    Destroy(rwall.gameObject);
            }

            if (!westExists)
            {
                var post = tile.CreatePost(postPrefab, BuildingWallFacing.NorthWest);
                if (post != null)
                {
                    posts.Add(post);
                }
            }
            else
            {
                var rwall = tile.RemovePost(BuildingWallFacing.NorthWest);
                posts.Remove(rwall);
                if (rwall != null)
                    Destroy(rwall.gameObject);
            }
        }
		else {
			var rwall = tile.RemovePost(BuildingWallFacing.NorthWest);
			posts.Remove(rwall);
			if (rwall != null)
				Destroy(rwall.gameObject);
			
			rwall = tile.RemovePost(BuildingWallFacing.NorthEast);
			posts.Remove(rwall);
			if (rwall != null)
				Destroy(rwall.gameObject);
		}

        if (!southExists)
        {
            if (!eastExists)
            {
                var post = tile.CreatePost(postPrefab, BuildingWallFacing.SouthEast);
                if (post != null)
                {
                    posts.Add(post);
                }
            }
            else
            {
                var rwall = tile.RemovePost(BuildingWallFacing.SouthEast);
                posts.Remove(rwall);
                if (rwall != null)
                    Destroy(rwall.gameObject);
            }

            if (!westExists)
            {
                var post = tile.CreatePost(postPrefab, BuildingWallFacing.SouthWest);
                if (post != null)
                {
                    posts.Add(post);
                }
            }
            else
            {
                var rwall = tile.RemovePost(BuildingWallFacing.SouthWest);
                posts.Remove(rwall);
                if (rwall != null)
                    Destroy(rwall.gameObject);
            }
        }
		else {
			var rwall = tile.RemovePost(BuildingWallFacing.SouthWest);
			posts.Remove(rwall);
			if (rwall != null)
				Destroy(rwall.gameObject);

			rwall = tile.RemovePost(BuildingWallFacing.SouthEast);
			posts.Remove(rwall);
			if (rwall != null)
				Destroy(rwall.gameObject);
		}

    }

    public GameObject CombineBuilding()
    {
        UpdateWalls();
        GameObject newBuilding =  TexturePacker.BuildMeshAndTextures(building);
        newBuilding.name = "House1";
        GameObject colliderParent = new GameObject("Colliders");
        colliderParent.transform.parent = newBuilding.transform;
        colliderParent.transform.localPosition = Vector3.zero;
        colliderParent.transform.localRotation = Quaternion.identity;

        Collider[] colliders = building.GetComponentsInChildren<Collider>();

        for (int cnt = 0; cnt < colliders.Length; cnt++)
        {
            if (colliders[cnt].isTrigger)
                continue;

            if (colliders[cnt] is BoxCollider)
            {
                var box = colliders[cnt] as BoxCollider;

                BoxCollider newBox = new GameObject("box").AddComponent<BoxCollider>();
                newBox.transform.position = box.transform.position;
                newBox.transform.rotation = box.transform.rotation;
                newBox.center = box.center;
                newBox.size = box.size;
                newBox.transform.parent = colliderParent.transform;
            }
        }

        

#if UNITY_EDITOR

        UnityEditor.AssetDatabase.CreateFolder("Assets", saveName);
        UnityEditor.AssetDatabase.CreateFolder("Assets/" + saveName, "Materials");
        UnityEditor.AssetDatabase.SaveAssets();

        MeshFilter[] meshFilters = newBuilding.GetComponentsInChildren<MeshFilter>();
        List<Material> materials = new List<Material>();
        for(int cnt = 0; cnt < meshFilters.Length; cnt++) {
            materials.AddRange(meshFilters[cnt].renderer.sharedMaterials);
            UnityEditor.AssetDatabase.CreateAsset(meshFilters[cnt].sharedMesh, "Assets/" + saveName + "/" + "Mesh" + cnt + ".asset");
        }

       // UnityEditor.AssetDatabase.CreateFolder("Assets/House1/", "Materials");
        for(int cnt = 0; cnt < materials.Count; cnt++) {
            if (materials[cnt].mainTexture != null)
            {
                UnityEditor.AssetDatabase.CreateAsset(materials[cnt].mainTexture, "Assets/" + saveName + "/Materials/" + saveName + cnt + ".asset");
            }

            UnityEditor.AssetDatabase.CreateAsset(materials[cnt], "Assets/" + saveName + "/Materials/" + saveName + materials[cnt].name + cnt + ".mat");
            
            
        }

        UnityEditor.PrefabUtility.CreatePrefab("Assets/" + saveName + "/" + saveName + ".prefab", newBuilding);

        UnityEditor.AssetDatabase.SaveAssets();
#endif
        return newBuilding;
    }
}

public enum BuildingEditMode
{
    Add,
    Destroy,
    Doors,
    Textures,
    Walls,
    DestroyWalls,
    WallTextures,
    Roofs,
}
