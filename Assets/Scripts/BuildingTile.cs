﻿using UnityEngine;
using System.Collections;

public class BuildingTile : MonoBehaviour {

    public GridClickable gridSpace;

    [HideInInspector]
    public BuildingWall northWall;
    [HideInInspector]
    public BuildingWall eastWall;
    [HideInInspector]
    public BuildingWall southWall;
    [HideInInspector]
    public BuildingWall westWall;

    [HideInInspector]
    public GameObject northEastPost;
    [HideInInspector]
    public GameObject northWestPost;
    [HideInInspector]
    public GameObject southEastPost;
    [HideInInspector]
    public GameObject southWestPost;

	public GameObject roof;

    //GameObject altWallNPrefab = null;
    //GameObject altWallEPrefab = null;
    //GameObject altWallSPrefab = null;
    //GameObject altWallWPrefab = null;


	// Use this for initialization
	void Start () {
        CreateWalls();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void CreateWalls()
    {

        northWall = new GameObject("WallN").AddComponent<BuildingWall>();
        northWall.transform.parent = transform;
        northWall.gameObject.layer = 15;
        northWall.tile = this;
        northWall.facing = BuildingWallFacing.North;
        northWall.transform.localPosition = new Vector3(0, 0, .5f);
        northWall.transform.localRotation = Quaternion.Euler(0, 180, 0);

        eastWall = new GameObject("WallE").AddComponent<BuildingWall>();
        eastWall.transform.parent = transform;
        eastWall.gameObject.layer = 15;
        eastWall.tile = this;
        eastWall.facing = BuildingWallFacing.East;
        eastWall.transform.localPosition = new Vector3(.5f, 0, 0);
        eastWall.transform.localRotation = Quaternion.Euler(0, -90, 0);

        southWall =  new GameObject("WallS").AddComponent<BuildingWall>();
        southWall.transform.parent = transform;
        southWall.gameObject.layer = 15;
        southWall.tile = this;
        southWall.facing = BuildingWallFacing.South;
        southWall.transform.localPosition = new Vector3(0, 0, -.5f);
        southWall.transform.localRotation = Quaternion.Euler(0, 0, 0);

        westWall = new GameObject("WallW").AddComponent<BuildingWall>();
        westWall.transform.parent = transform;
        westWall.gameObject.layer = 15;
        westWall.tile = this;
        westWall.facing = BuildingWallFacing.West;
        westWall.transform.localPosition = new Vector3(-.5f, 0, 0);
        westWall.transform.localRotation = Quaternion.Euler(0, 90, 0);

    }

    public BuildingWall CreateWall(GameObject prefab, BuildingWallFacing facing)
    {

        switch (facing)
        {
            case BuildingWallFacing.North:
                if (northWall != null)
                {
                    gridSpace.buildingGrid.RemoveWall(northWall);
                }

                //if(northWall == null) {
                northWall.CreateWall(prefab);
                //}
                return northWall;
            case BuildingWallFacing.East:
                if (eastWall != null)
                {
                    gridSpace.buildingGrid.RemoveWall(eastWall);
                }

               // if (eastWall == null)

                    eastWall.CreateWall(prefab);
                
                return eastWall;
            case BuildingWallFacing.South:
                if (southWall != null)
                {
                    gridSpace.buildingGrid.RemoveWall(southWall);
                }

                    southWall.CreateWall(prefab);
                
                return southWall;
            case BuildingWallFacing.West:
                if (westWall != null)
                {
                    gridSpace.buildingGrid.RemoveWall(westWall);
                }


                    westWall.CreateWall(prefab);
                
                return westWall;

        }

        return null;
    }

    public BuildingWall RemoveWall( BuildingWallFacing facing) {
        switch (facing)
        {
            case BuildingWallFacing.North:
                northWall.RemoveWalls();
                return northWall;
            case BuildingWallFacing.East:
                eastWall.RemoveWalls();
                return eastWall;
            case BuildingWallFacing.South:
                southWall.RemoveWalls();
                return southWall;
            case BuildingWallFacing.West:
                westWall.RemoveWalls();
                return westWall;
        }

        return null;
    }

    public GameObject CreatePost(GameObject prefab, BuildingWallFacing facing)
    {
        switch (facing)
        {
            case BuildingWallFacing.NorthEast:
                if (northEastPost == null)
                {
                    northEastPost = Instantiate(prefab) as GameObject;
                    northEastPost.transform.parent = transform;
                    northEastPost.transform.localPosition = new Vector3(.5f, 0, .5f);
                    northEastPost.transform.localRotation = Quaternion.Euler(0, -90, 0);
                }
                return northEastPost;
            case BuildingWallFacing.NorthWest:
                if (northWestPost == null)
                {
                    northWestPost = Instantiate(prefab) as GameObject;
                    northWestPost.transform.parent = transform;
                    northWestPost.transform.localPosition = new Vector3(-.5f, 0, .5f);
                    northWestPost.transform.localRotation = Quaternion.Euler(0, 180, 0);
                }
                return northWestPost;
            case BuildingWallFacing.SouthEast:
                if (southEastPost == null)
                {
                    southEastPost = Instantiate(prefab) as GameObject;
                    southEastPost.transform.parent = transform;
                    southEastPost.transform.localPosition = new Vector3(.5f, 0, -.5f);
					southEastPost.transform.localRotation = Quaternion.Euler(0, 0, 0);
                }
                return southEastPost;
            case BuildingWallFacing.SouthWest:
                if (southWestPost == null)
                {
                    southWestPost = Instantiate(prefab) as GameObject;
                    southWestPost.transform.parent = transform;
                    southWestPost.transform.localPosition = new Vector3(-.5f, 0, -.5f);
                    southWestPost.transform.localRotation = Quaternion.Euler(0, 90, 0);
                }
                return southWestPost;
        }

        return null;
    }

    public GameObject RemovePost(BuildingWallFacing facing)
    {
        switch (facing)
        {
            case BuildingWallFacing.NorthEast:

                return northEastPost;
            case BuildingWallFacing.NorthWest:

                return northWestPost;
            case BuildingWallFacing.SouthEast:

                return southEastPost;
            case BuildingWallFacing.SouthWest:

                return southWestPost;
        }

        return null;
    }

    public void ChangeWallPrefab(GameObject wallPrefab, BuildingWallFacing facing, GameObject clickedWall)
    {

        switch (facing)
        {
            case BuildingWallFacing.North:

                northWall.ChangeWallType(wallPrefab, clickedWall);
                break;
            case BuildingWallFacing.East:
                eastWall.ChangeWallType(wallPrefab, clickedWall );
                break;
            case BuildingWallFacing.South:
                southWall.ChangeWallType(wallPrefab, clickedWall);
                break;
            case BuildingWallFacing.West:
                westWall.ChangeWallType(wallPrefab, clickedWall);
                break;
        }

    }

    public void ChangeWallTexture(Texture wallTexture, BuildingWallFacing facing, GameObject clickedWall)
    {

        switch (facing)
        {
            case BuildingWallFacing.North:

                northWall.ChangeWallTexture(wallTexture, clickedWall);
                break;
            case BuildingWallFacing.East:
                eastWall.ChangeWallTexture(wallTexture, clickedWall);
                break;
            case BuildingWallFacing.South:
                southWall.ChangeWallTexture(wallTexture, clickedWall);
                break;
            case BuildingWallFacing.West:
                westWall.ChangeWallTexture(wallTexture, clickedWall);
                break;
        }

    }

    public void RemoveWallPrefab(BuildingWallFacing facing)
    {
        ChangeWallPrefab(null, facing, null);
    }

    public void RemoveWallPrefabIfSame(GameObject wallPrefab, BuildingWallFacing facing)
    {
        switch (facing)
        {
            case BuildingWallFacing.North:

                northWall.RemoveWallPrefabIfSame(wallPrefab);
                break;
            case BuildingWallFacing.East:
                eastWall.RemoveWallPrefabIfSame(wallPrefab);
                break;
            case BuildingWallFacing.South:
                southWall.RemoveWallPrefabIfSame(wallPrefab);
                break;
            case BuildingWallFacing.West:
                westWall.RemoveWallPrefabIfSame(wallPrefab);
                break;
        }
    }

    public void RemoveWallTextureIfSame(Texture wallTexture, BuildingWallFacing facing)
    {
        switch (facing)
        {
            case BuildingWallFacing.North:

                northWall.RemoveWallTextureIfSame(wallTexture);
                break;
            case BuildingWallFacing.East:
                eastWall.RemoveWallTextureIfSame(wallTexture);
                break;
            case BuildingWallFacing.South:
                southWall.RemoveWallTextureIfSame(wallTexture);
                break;
            case BuildingWallFacing.West:
                westWall.RemoveWallTextureIfSame(wallTexture);
                break;
        }
    }
}

public enum BuildingWallFacing {
    North,
    East,
    South,
    West,
    NorthEast,
    NorthWest,
    SouthEast,
    SouthWest
}
